\documentclass[11pt]{article}

%\usepackage[T1]{fontenc}

\usepackage{latexsym}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{amscd}
\usepackage{epsfig}
\usepackage{verbatim}
%\usepackage{fancybox}
\usepackage{moreverb}
\usepackage{graphicx}
%\usepackage{psfrag}
\usepackage{hyperref}
\usepackage{bm}
\usepackage{appendix}

\usepackage{url}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{algorithm}% http://ctan.org/pkg/algorithm
\usepackage{algpseudocode}% http://ctan.org/pkg/algorithmicx

\usepackage{xcolor}
\definecolor{commentColour}{rgb}{0,0.6,0}
\definecolor{mGray}{rgb}{0.5,0.5,0.5}
\definecolor{mPurple}{rgb}{0.58,0,0.82}
\definecolor{backgroundColour}{rgb}{0.95,0.95,0.92}


\lstdefinestyle{CStyle}{
    language=C,
    basicstyle=\small\ttfamily,
    backgroundcolor=\color{backgroundColour},   
    commentstyle=\color{commentColour}\sffamily,
    keywordstyle=\color{blue},
    numberstyle=\tiny\color{mGray},
    stringstyle=\color{mPurple},
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2,
    morekeywords={omp,parallel,private,firstprivate,shared,schedule,exit,static,dynamic,guided,runtime}
}



\textheight 22cm    \textwidth 16cm
\voffset=-1cm
\hoffset=-1.2cm

\newcommand{\C}{{\mathbb C}}
\newcommand{\R}{{\mathbb R}}
\newcommand{\N}{{\mathbb N}}
\newcommand{\Z}{{\mathbb Z}}
\newcommand{\Q}{{\mathbb Q}}
\newcommand{\T}{{\mathbb T}}
\newcommand{\E}{{\mathbb E}}
\newcommand{\di}{{\mathbb D}}
\newcommand{\Y}{{\mathbf Y}}
\newcommand{\D}{{\partial}}
\newcommand{\Cl}{{\mathcal C}}
\newcommand{\Pa}{{\mathcal P}}
\newcommand{\Flux}{{\mathcal F}}
\newcommand{\B}{{\mathfrak B}}
\newcommand{\M}{{\mathcal M}}
\newcommand{\dis}{{\mathcal D}}
\newcommand{\A}{{\mathcal{A}}}
\newcommand{\fin}{\rule{1ex}{1ex}}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{hypothesis}[theorem]{Hypothesis}

\renewcommand{\thesection}{\arabic{section}}
\renewcommand{\thelemma}{\thesection\arabic{lemma}}
\renewcommand{\theproposition}{\thesection\arabic{proposition}}
\renewcommand{\thecorollary}{\thesection\arabic{corollary}}
\renewcommand{\thedefinition}{\thesection\arabic{definition}}
\renewcommand{\theexample}{\thesection\arabic{example}}
\renewcommand{\theremark}{\thesection\arabic{remark}}
\renewcommand{\thenotation}{\thesection\arabic{notation}}
\renewcommand{\theequation}{\thesection.\arabic{equation}}
\def\commutatif{\ar@{}[rd]|{\circlearrowleft}}

%\title{Parallel generation of fractals}

%\author{Master 2 - Scientific Computing}

\date{}

\begin{document}

\begin{flushright}
{\Large \it Universit\'e de Lille \hfill{Master 2 - Scientific Computing, 2024-2025}}
\end{flushright}

\vspace{0.4cm} \hrule \hrule

\begin{center}
{\LARGE \bf Project: Parallel Mandelbrot set-based generation of fractals}
\end{center}

\begin{center}
\Large \it N. Melab, A. Mouton and J. Gmys
\end{center}

\hrule \hrule

\vspace{1cm}

%\maketitle

\textit{The aim of this project is to use various parallelization techniques by using OpenMP, MPI and Cuda, in order to provide a graphical representation of Mandelbrot's fractal. Each student is expected to return a written report and a tar.gz or .zip file containing the codes associated to the report. The report should contain some explainations on the implementation techniques that are used and should also present some performance results with comments. Both report and code should be sent by e-mail to J. Gmys (jan.gmys@univ-lille.fr), N. Melab (nouredine.melab@univ-lille.fr) and A. Mouton (alexandre.mouton@univ-lille.fr) before \underline{January 6th at 23:59 pm}.}

\section{Introduction}

Discovered during the XIX$^{\textnormal{th}}$ century, fractals have been considered as strange mathematical objects until the middle of XX$^{\textnormal{th}}$ century and the work of B. Mandelbrot who put them as the center of a new branch of mathematics: the fractal geometry. The word \textit{fractal} has been itself introduced by Mandelbrot for pointing out a geometric object which presents some irregularity properties roughly different from usual euclidian geometric objects like circles, polygons or lines. Indeed, Mandelbrot defined a fractal as a geometric object which remains invariant through any scale change. In order to produce a fractal image, we generally start from a graphical object, and apply to this object a transformation which adds a new element to its complexity. Then, we apply one more time the same transformation to this new object for complexifying it, and we loop like this as far as we want.

\section{Mandelbrot' set}

Mandelbrot' set is a particular fractal object discovered by B. Mandelbrot in 1980 and, although presenting a infinite complexity, this subset of $\C$ is quite simple to generate thanks to a small recurrence formula in $\C$.

\begin{definition}
Mandelbrot' set is defined as the complex numbers $c \in \C$ such that the sequence $(z_{n})_{n\,\in\,N} \subset \C$ defined by
\begin{equation}
z_{0} = 0 \, , \qquad z_{n+1} = z_{n}^{2}+c \, ,
\end{equation}
remains bounded in $\C$.
\end{definition}

In order to represent this fractal in the subset $[x_{min},x_{max}] \times [y_{min},y_{max}] \subset \Omega$\footnote{$\Omega=[-1.78,0.78]\times[-0.96,0.96]$.}, we start from each point $(a,b)$ of the picture, we define $c = a+ib \in \C$ and the sequence $(z_{n})_{n\,\in\,\N}$, and we finally define $I(a,b)$ as the luminous intensity with the smaller integer $n$ required for observing the divergence of $(z_{n})_{n\,\in\,\N}$. In practical cases, we consider an integer $N\,\in\,N^{*}$ fixed for the whole picture and $I(a,b)$ is defined as follows:
\begin{equation}
I(a,b) = \cfrac{1}{N}\,\min_{n\,=\,0,\dots,N} \left\{ n\,:\, |z_{n}| > 2\right\} \, .
\end{equation}

\subsection{Sequential implementation}

You can find a sequential C program along with the present document in the \texttt{FinalProject} folder on the following webpage: \\

\url{https://gitlab.univ-lille.fr/jan.gmys/m2-supercomputing/}
\vspace{0.5cm}

%\url{https://gitlab.inria.fr/jgmys/m2chps-supercomputing/-/blob/master/Final_Project/mandel.c}
%\indent \verb|https://sites.google.com/site/moutonalexandre/teaching/supercomputing| \\
%

	
%\begin{algorithm}[!htbp]
%\scriptsize
%\caption{Compute Mandelbrot}\label{alg:GPU}
%\begin{algorithmic}[1]
%%%%%%%%%%%%%%%%%%%%
%\Procedure{Compute}{}
%\For{k : $1\to m$}
%\State aaa
%\EndFor
%\EndProcedure
%%%%%%%%%%%%%%%%%%%%
%\end{algorithmic}
%%\end{spacing}
%\end{algorithm}
%


\indent \verb|For| $a$ \verb|from| $x_{min}$ \verb|to| $x_{max}$ \\
\indent \indent \verb|For| $b$ \verb|from| $y_{min}$ \verb|to| $y_{max}$ \\
\indent \indent \indent $x = y = 0$ \\
\indent \indent \indent $i = 0$ \\
\indent \indent \indent \verb|While| $x^{2}+y^{2} < 4$ \verb|and| $i < N$ \\
\indent \indent \indent \indent $t = x$ \\
\indent \indent \indent \indent $x = x^{2}+y^{2}+a$ \\
\indent \indent \indent \indent $y = 2ty+b$ \\
\indent \indent \indent \indent $i = i+1$ \\
\indent \indent \indent \verb|End While| \\
\indent \indent \indent $I(a,b) = i/N$ \\
\indent \indent \verb|End For| \\
\indent \verb|End For| \\

This program requires 4 arguments for execution:
\begin{itemize}
\item The value of $N$ (default value: 100),
\item The values $x_{min}$, $x_{max}$, $y_{min}$, $y_{max}$ which define the represented subdomain of $\Omega$ (default values: $\Omega=[-1.78,0.78]\times[-0.96,0.96]$),
\item The dimensions of the produced picture expressed in pixels (default values: $1024\times768$ pixels),
\item The name of the produced \verb|.ppm| picture (default value: \verb|/tmp/mandel.ppm|).
\end{itemize}

Some indications about the specifications of these parameters can be obtained using the following command:
\begin{verbatim}
./mandel --help
\end{verbatim}

Perform several runs of the program with specified bounds, pictures dimensions or values of $N$.

\begin{verbatim}
./mandel -f /tmp/mandel1.ppm
./mandel -b -0.56 -0.53 0.6 0.63 -n 150 -f /tmp/mandel2.ppm
...
\end{verbatim}

Open the file \verb|mandel.c| and analyze its structure. Its \verb|main| function is essentially constituted of four steps:
\begin{enumerate}
\item The analysis of arguments given when the program is executed,
\item The initialization of the image,
\item The computation of $I(a,b)$ for each pixel $(a,b)$,
\item The save of the picture to PPM format.
\end{enumerate}

In order to produce high quality fractal pictures, it is necessary to consider a very high number of pixels along with a high value of $N$. Indeed, it has been proved that Mandelbrot' set is connected in $\C$ (Douady \& Hubbard - 1984). However, considering a low resolution treatment leads to the appearance of isolated points or subsets within the graphical representation. As a consequence, a good graphical representation of Mandelbrot' set can require huge CPU resources because of a high number of pixels and/or a high value of $N$. This constitutes the main motivation of implementing this graphical representation by using parallel computing.

\begin{figure}[ht]
\begin{center}
\fbox{\includegraphics[width=0.4\textwidth]{mandel.pdf}} \hspace{0.4cm}\fbox{\includegraphics[width=0.4\textwidth]{mandelzoom.pdf}}
\caption{\label{fig:figure1}Mandelbrot set on $\Omega$ (left) and $[-0.56,-0.53]\times[0.6,0.63]$ (right) with $1024\times768$ resolution and a convergence threshold $N=150$.}
\end{center}
\end{figure}

\begin{figure}[ht]
\begin{center}
\fbox{\includegraphics[width=0.3\textwidth]{mandel_100k.png}}\hspace{0.1cm}\fbox{\includegraphics[width=0.3\textwidth]{mandel_500k.png}}\hspace{0.1cm}\fbox{\includegraphics[width=0.3\textwidth]{mandel_1M.png}}
\caption{\label{fig:figure2}Mandelbrot set on $[0.37500012006,0.375000120064]\times[-0.216639388440,-0.216639388436]$ with $1024\times1024$ resolution and convergence thresholds $N=100000$, $500000$ and $1000000$ respectively.}
\end{center}
\end{figure}

Figure~\ref{fig:figure1} shows the images generated for the default domain $\Omega$ and a zoom on a smaller subdomain, both using the default resolution $1024\times768$ and $N=150$.
Figure~\ref{fig:figure2} shows a zoom on a square domain whose sides are about \textit{one billion times} smaller that the sides of a single pixel in Figure~\ref{fig:figure1} (left). As one can notice, even with $N=100000$ all points seem to belong to the set and a very high convergence threshold is necessary to see details of the image emerge.



\section{First approach using MPI: block partitioning}

A quite simple approach consists in giving to each CPU the computation of a block made of contiguous lines. Once such partial pictures are computed, they are collected and gathered by a master process which finally saves the global picture.

\begin{enumerate}
\item Build this parallel program by using the routine \verb|MPI_Gather|.
\item Study and plot the speed-up\footnote{Reminder: the speed-up of a parallel program is the ratio between the execution time of the best sequential version of the code and the time of the parallel version of the code. A good speed-up is diagnosed if this ratio grows up along with the number of CPUs.} of your parallel program according to the number of involved CPUs. Conclusions ?
\item Modify the sequential version of the code in order to print the required CPU time for building each line (see Appendix). Plot the corresponding curve and conclude about the load balancing between the processors.
\end{enumerate}

\paragraph{Some Hints}
\begin{itemize}
\item Only \verb|main| function has to be modified,
\item All CPUs (including CPU 0) take part in the computations,
\item In order to simplify the parallel implementation, the vertical dimension given by the user can be assumed to be a multiple of the number of involved CPUs.
\begin{verbatim}
MPI_Comm_Size(MPI_COMM_WORLD, & size);
...
height = height/size * size;
\end{verbatim}
\end{itemize}

\section{Second approach using MPI: interlacing lines}

The goal is to improve the load balancing between the processors. For this purpose, we choose to replace the assignment of a block made of contiguous lines to each processor by the assignment of a block made of alternated lines. To be precise, we will assume that the $k$-th processor ($k \in [0,n[$) takes in charge the lines indexed by $k+i\,n$. \\
\indent For each following point, some implementations, tests and speed-up studies are expected:
\begin{enumerate}
\item Each CPU computes a line and sends this partial result before computing the next line. Such an approach may be based on routines \verb|MPI_Send|, \verb|MPI_Recv|, ...
\item Improve the previous code by using nonblocking functions like \verb|MPI_Isend|, \verb|MPI_Wait|, ... in order to perform computations and communications at the same time.
\item Each CPU computes a set of lines, then sends this set to the master process in a unique message. This master process will have to receive and reorganize the data. The routines \verb|MPI_Pack|, \verb|MPI_Unpack|, \verb|MPI_Send|... may be used.
\end{enumerate}

\section{Third approach using OpenMP for multi-core processors}
\begin{enumerate}
\item Design and implement the parallel generation of fractals using OpenMP considering two parallelization models: {\it master-worker} and {\it work pool}.\\
{\it NB}: recall that the {\it master-worker} (resp. {\it work pool}) model can be exploited in OpenMP in a transparent way by simply using {\it static} (resp. {\it dynamic}) as a parameter of the {\it schedule} clause.
\item Compare the speed-ups obtained with the two parallelization models. Also consider different chunk sizes (try at least two different chunk sizes for each model).
\item Choose a large enough configuration (image size, precision $N$, image bounds) such that the sequential image generation (excluding I/O, i.e. excluding the of saving of the image) takes at least one minute and study the speedup on one of the computing platforms you have access to (Grid5000 and/or MesoNET).  
\item Compare to the previous MPI approach.
\end{enumerate}

\section{Fourth approach using Cuda for GPU accelerators}
\begin{enumerate}
\item Design and implement the parallel generation of fractals on GPU using Cuda. 
%\item Experiment the implementation on the GPU card of your laptop, then on a Cuda card provided in Grid'5000.
\item Measure the time spent for computing the fractal on the GPU and for copying the image to the CPU separately. 
\item Study the performance of the Cuda program for the default configuration different precisions/convergence thresholds ($N$) and image sizes.
\item Compare to the previous OpenMP and MPI approaches.
\end{enumerate}

\section{Fifth approach using hybrid programming :  MPI-Cuda and MPI-OpenMP (Optional)}
\begin{enumerate}
\item Design and implement the parallel generation of fractals using hybrid programming MPI-OpenMP and study the speed-up using multiple nodes on Grid'5000 or MesoNET (each MPI process will be multi-threaded; typically there is one MPI process per compute node).
\item Design and implement the parallel generation of fractals using hybrid programming MPI-Cuda and compare with the single-GPU approach (the goal is to use multiple GPUs, each MPI process using one device).
\item Design and implement the parallel generation of fractals using the three parallel programming together : MPI-OpenMP-Cuda. The goal is to design a fractal generator that exploits all the resources (CPU cores and GPU devices) of multiple GPU-equipped nodes (such as the \texttt{chifflot} cluster on the Grid'5000 site in Lille).  
\end{enumerate}


\section{Appendix: measure of elapsed time}

In order to perform of measurement of elapsed time in a C program, you can use the function \verb|clock_gettime| as follows:
\begin{lstlisting}[style=CStyle,caption={time measurement}]
#include <time.h>

struct timespec tstart, tend;
clock_gettime (CLOCK_MONOTONIC, &tstart);
/*
... My code ...
*/
clock_gettime (CLOCK_MONOTONIC, &tend);
double elap_time=(tend.tv_sec-tstart.tv_sec)+(tend.tv_nsec-tstart.tv_nsec)/1e9f;
printf("Elapsed time (seconds): %2.9lf\n",elap_time);
\end{lstlisting}

\end{document}
