#include "mmul_kernel.cuh"


__global__
void
matmul2D(float * A, float * B, float * C, int size)
{
    unsigned int row = blockIdx.y * blockDim.y + threadIdx.y;
    unsigned int col = blockIdx.x * blockDim.x + threadIdx.x;

    float val = 0.;

    if (row >= size || col >= size) return;

    for (int k = 0; k < size; k++)
        val += A[row * size + k] * B[k * size + col];

    C[row * size + col] = val;
}

////////////////////////////////////////////
// kernel using shared memory
__global__
void
matmul2D_smem(float * A, float * B, float * C, int size)
{
    __shared__ float A_s[TILE_WIDTH][TILE_WIDTH];
    __shared__ float B_s[TILE_WIDTH][TILE_WIDTH];

    int bx = blockIdx.x;
    int by = blockIdx.y;
    int tx = threadIdx.x;
    int ty = threadIdx.y;

    int col = bx * TILE_WIDTH + tx;
    int row = by * TILE_WIDTH + ty;

    if (row >= size || col >= size) return;

    float val = 0.;

    int indA = row * size + tx;
    int indB = ty * size + col;

    // loop over tiles
    for (int m = 0; m < (size + TILE_WIDTH - 1) / TILE_WIDTH; m++) {
        // load
        A_s[ty][tx] = A[indA];
        B_s[ty][tx] = B[indB];
        __syncthreads();

        indA += TILE_WIDTH;
        indB += TILE_WIDTH * size;

        for (int k = 0; k < TILE_WIDTH; ++k)
            val += A_s[ty][k] * B_s[k][tx];

        __syncthreads();
    }

    C[row * size + col] = val;
} // matmul2D_smem

///////////////////////////////////////////
__global__
void
matmul2D_smem_24(float * A, float * B, float * C, int size)
{
    __shared__ float A_s[TILE_WIDTH][TILE_WIDTH];
    __shared__ float B_s[TILE_WIDTH][TILE_WIDTH];

    int s = TILE_WIDTH / 2;// tx stride
    int t = 1;// TILE_WIDTH/4;//ty stride

    int bx = blockIdx.x;
    int by = blockIdx.y;
    // int tx = 2*threadIdx.x; int ty = 4*threadIdx.y;
    int tx = threadIdx.x;
    int ty = 4 * threadIdx.y;

    int col = bx * TILE_WIDTH + tx;
    int row = by * TILE_WIDTH + ty;

    if (row >= size || col >= size) return;

    float val  = 0.;
    float val2 = 0.;
    float val3 = 0.;
    float val4 = 0.;

    float val5 = 0.;
    float val6 = 0.;
    float val7 = 0.;
    float val8 = 0.;

    // indices to first elem to load from gmem (A,B) by thread
    int indA = row * size + tx;
    int indB = ty * size + col;

    // loop over tiles
    for (int m = 0; m < (size + TILE_WIDTH - 1) / TILE_WIDTH; m++) {
        // load
        A_s[ty][tx]         = A[indA];
        A_s[ty + t][tx]     = A[indA + t * size];
        A_s[ty + 2 * t][tx] = A[indA + 2 * t * size];
        A_s[ty + 3 * t][tx] = A[indA + 3 * t * size];

        A_s[ty][tx + s]         = A[indA + s];
        A_s[ty + t][tx + s]     = A[indA + t * size + s];
        A_s[ty + 2 * t][tx + s] = A[indA + 2 * t * size + s];
        A_s[ty + 3 * t][tx + s] = A[indA + 3 * t * size + s];

        B_s[ty][tx]         = B[indB];
        B_s[ty + t][tx]     = B[indB + t * size];
        B_s[ty + 2 * t][tx] = B[indB + 2 * t * size];
        B_s[ty + 3 * t][tx] = B[indB + 3 * t * size];

        B_s[ty][tx + s]         = B[indB + s];
        B_s[ty + t][tx + s]     = B[indB + t * size + s];
        B_s[ty + 2 * t][tx + s] = B[indB + 2 * t * size + s];
        B_s[ty + 3 * t][tx + s] = B[indB + 3 * t * size + s];

        __syncthreads();

        // advance one tile
        indA += TILE_WIDTH;
        indB += TILE_WIDTH * size;
        //
        int k;
        //    #pragma unroll
        for (k = 0; k < TILE_WIDTH; ++k) {
            val  += A_s[ty][k] * B_s[k][tx];
            val2 += A_s[ty + t][k] * B_s[k][tx];
            val5 += A_s[ty + 2 * t][k] * B_s[k][tx];
            val6 += A_s[ty + 3 * t][k] * B_s[k][tx];

            val3 += A_s[ty][k] * B_s[k][tx + s];
            val4 += A_s[ty + t][k] * B_s[k][tx + s];
            val7 += A_s[ty + 2 * t][k] * B_s[k][tx + s];
            val8 += A_s[ty + 3 * t][k] * B_s[k][tx + s];
        }

        __syncthreads();
    }

    C[row * size + col]           = val;
    C[(row + t) * size + col]     = val2;
    C[row * size + col + s]       = val3;
    C[(row + t) * size + col + s] = val4;

    C[(row + 2 * t) * size + col]     = val5;
    C[(row + 3 * t) * size + col]     = val6;
    C[(row + 2 * t) * size + col + s] = val7;
    C[(row + 3 * t) * size + col + s] = val8;
} // matmul2D_smem_24
