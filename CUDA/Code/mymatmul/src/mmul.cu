#include <stdio.h>
#include <time.h>

#include "mmul_host.h"
#include "mmul_kernel.cuh"


int
main(int argc, char ** argv)
{
    if (argc < 4) {
        printf("provide 3 arguments:\n ./mmul [SIZE] [compare vs. CPU (0/1)] [kernel version (1-2-3)]\n");
        exit(0);
    }

    // SIZE?
    int N = 64;
    N = atoi(argv[1]);
    printf("*** matmul with N=%d ***\n", N);

    // matmul on CPU?
    int host = 1;
    host = atoi(argv[2]);

    int kernel = 1;
    kernel = atoi(argv[3]);

    float * A, * B, * C, * D;// host data
    float * A_d, * B_d, * C_d; // device data

    // allocate host data
    A = (float *) malloc(N * N * sizeof(float));
    B = (float *) malloc(N * N * sizeof(float));
    C = (float *) malloc(N * N * sizeof(float));
    D = (float *) malloc(N * N * sizeof(float));

    // fill A,B randomly
    srand(time(NULL));
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            A[i * N + j] = rand() % 10;
            B[i * N + j] = rand() % 10;
        }
    }

    // timer variables
    struct timespec start, end;
    float elapsed;
    cudaEvent_t start_ev, stop_ev;
    gpuErrchk(cudaEventCreate(&start_ev) );
    gpuErrchk(cudaEventCreate(&stop_ev) );

    // ================================== CPU
    if (host) {
        clock_gettime(CLOCK_MONOTONIC, &start);
        matmulHost(A, B, D, N);
        clock_gettime(CLOCK_MONOTONIC, &end);

        // in sec
        elapsed = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1000000000.;

        printf("...CPU elapsed (ms):\t\t   %.5f\n\n", 1000. * elapsed);
    }

    // ================================== GPU
    // allocate on device
    gpuErrchk(cudaMalloc(&A_d, N * N * sizeof(float)) );
    gpuErrchk(cudaMalloc(&B_d, N * N * sizeof(float)) );
    gpuErrchk(cudaMalloc(&C_d, N * N * sizeof(float)) );

    // copy to device
    gpuErrchk(cudaEventRecord(start_ev) );
    gpuErrchk(cudaMemcpy(A_d, A, N * N * sizeof(float), cudaMemcpyHostToDevice) );
    gpuErrchk(cudaMemcpy(B_d, B, N * N * sizeof(float), cudaMemcpyHostToDevice) );
    gpuErrchk(cudaMemcpy(C_d, C, N * N * sizeof(float), cudaMemcpyHostToDevice) );
    gpuErrchk(cudaEventRecord(stop_ev) );
    gpuErrchk(cudaEventSynchronize(stop_ev) );
    gpuErrchk(cudaEventElapsedTime(&elapsed, start_ev, stop_ev) );

    printf("Memcpy H->D (ms):\t\t %f\n\n", elapsed);

    // configure blocks / blocksize
    dim3 blocksize(TILE_WIDTH, TILE_WIDTH);
    dim3 blocks((N + TILE_WIDTH - 1) / TILE_WIDTH, (N + TILE_WIDTH - 1) / TILE_WIDTH);
    printf("Kernel %d -- config: (%d,%d)x(%d,%d)\n", kernel, blocks.x, blocks.y, blocksize.x, blocksize.y);
    // compute on GPU
    gpuErrchk(cudaEventRecord(start_ev) );
    switch (kernel) {
        case 1:
            matmul2D << < blocks, blocksize >> > (A_d, B_d, C_d, N);
            gpuErrchk(cudaEventRecord(stop_ev) );
            gpuErrchk(cudaEventSynchronize(stop_ev) );
            gpuErrchk(cudaEventElapsedTime(&elapsed, start_ev, stop_ev) );
            gpuErrchk(cudaPeekAtLastError() );
            gpuErrchk(cudaDeviceSynchronize() );

            break;
        case 2:
            matmul2D_smem << < blocks, blocksize >> > (A_d, B_d, C_d, N);
            gpuErrchk(cudaEventRecord(stop_ev) );
            gpuErrchk(cudaEventSynchronize(stop_ev) );
            gpuErrchk(cudaEventElapsedTime(&elapsed, start_ev, stop_ev) );
            gpuErrchk(cudaPeekAtLastError() );
            gpuErrchk(cudaDeviceSynchronize() );
            break;
        case 3:
            blocksize.x /= 2;
            blocksize.y /= 4;

            matmul2D_smem_24 << < blocks, blocksize >> > (A_d, B_d, C_d, N);
            gpuErrchk(cudaEventRecord(stop_ev) );
            gpuErrchk(cudaEventSynchronize(stop_ev) );
            gpuErrchk(cudaEventElapsedTime(&elapsed, start_ev, stop_ev) );
            gpuErrchk(cudaPeekAtLastError() );
            gpuErrchk(cudaDeviceSynchronize() );
            break;
    }


    printf("Kernel Time (ms)\t:\t%.5f \n", elapsed);

    // copy results back to host
    cudaMemcpy(C, C_d, N * N * sizeof(int), cudaMemcpyDeviceToHost);
    if (host) {
        if (compareResults(C, D, N, 1.0e-6)) printf("\ncorrectness check ok\n");
        else printf("CPU != GPU :(\n");
    }

    // free memory on host
    free(A);
    free(B);
    free(C);
    free(D);

    // free memory on device
    gpuErrchk(cudaFree(A_d) );
    gpuErrchk(cudaFree(B_d) );
    gpuErrchk(cudaFree(C_d) );

    // reset device
    gpuErrchk(cudaDeviceReset() );
} // main
