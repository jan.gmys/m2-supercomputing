#ifndef MMUL_HOST_H
#define MMUL_HOST_H

void
matmulHost(const float * A, const float * B, float * C, int size);

bool
compareResults(const float * arrA, const float * arrB, const int size, const float eps);

#endif
