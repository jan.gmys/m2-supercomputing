#include <stdio.h>

int
main()
{
    #pragma omp parallel
    {
        printf("hello ");
        printf("openmp!\n");
    }
}
