program primes
implicit none
integer(kind=4)::i,j,prime,total=0
integer(kind=4),parameter::n=100000

do i=2,n
    prime=1
    do j=2,i-1
        if(mod(i,j)==0)then
           prime=0
           exit
        endif
    end do
    total=total+prime
end do

print*,total

end program primes
