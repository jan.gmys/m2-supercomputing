# Welcome to the Supercomputing practical works of the Master Scientific Computing (ULille)


## Planning 2024 - 2025 (**PART 1** of the practical works)

### Grid5000  (1 session)
- Oct 16, 09:00-12:00
- [Tutorial : Getting started with Grid'5000](Grid5000/tp_g5k.pdf)


### OpenMP (2 sessions)
- Oct 17, 9:00-12:00
- Oct 21, 14:00-17:00
- [Tutorial : shared memory programming with OpenMP](OpenMP/omp_C2024.pdf)

### CUDA (2 Sessions)
- Oct 23, 09:00-12:00
- Oct 25, 09:00-12:00
- [Tutorial : GPU programming with CUDA](CUDA/CudaTuto.pdf)
